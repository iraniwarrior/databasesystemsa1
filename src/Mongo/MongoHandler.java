package Mongo;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

import org.bson.Document;
import org.bson.types.ObjectId;

import Heap.BusinessRecord;
import Heap.HeapField;
import Heap.HeapFieldSizes;

public class MongoHandler {
	
	private Mongo mongo = new Mongo();
	
	public void insertBusiness(String line) {
    	String splitLines[] =  line.split("\t", -1);
    	HeapFieldSizes heapFieldSizes = new HeapFieldSizes();
    	HeapField heapField = null;
    	String fieldName = "";
    	BusinessRecord businessRecord = new BusinessRecord();
    	//loop through the tokenized fields, start at 1 because the first element is not needed    	
    	for(int x=0;x<splitLines.length;x++) {
    		//skip column 0 as that is irrelevant information
    		if(x != 0 ) {    		
    			//Get the heapField which stores information about the current tokenized index)
    			heapField = heapFieldSizes.getHeapFieldByIndex(x);
    			//get the fieldName
    			fieldName = heapField.getName();
    			//we have our value, put it in our businessRecord Object			
    			businessRecord.setRecordField(fieldName, splitLines[x]);
    			
    		}
    	}

    	String previousState = businessRecord.getPreviousState();	

    	
    	ObjectId objectPrevState = null;
    	
    	
    	//this replaces the prev state string to the state Id from our database
    	if(previousState != null && !previousState.isEmpty()) {
    		previousState = previousState.toUpperCase();
    		objectPrevState = mongo.getStates().get(previousState);
    	}	
    	
	    
	    Document doc = new Document ("businessName", businessRecord.getBusinessName())
	            .append("RegistrationDetails", new Document("status", businessRecord.getStatus())
	            		.append("dateOfRegistration", businessRecord.getRegistrationDate())
	            		.append("dateOfCancellation", businessRecord.getCancellationDate())
	            		.append("renewalDate", businessRecord.getRenewalDate())
	            		)
	            .append("formerStateNumber", businessRecord.getFormerStateNumber())
	            .append("previousState", objectPrevState)
	            .append("abn", businessRecord.getAbn());

	    
	    mongo.insertObject(doc);    	
    	
    	
	}
	public void populateDatabase(String filename) {

		
		try {
			String line = null;
			FileReader filereader = new FileReader(filename);
			
			mongo.createConnection("DatabaseSystemsA1");
			mongo.emptyDocument();	
			mongo.populateStates();
			
            BufferedReader bufferedReader = new BufferedReader(filereader);
            int numberOfRows = 0;            

            //read line by line
            while((line = bufferedReader.readLine()) != null) {
                
       
            	//first line is just row name data so we skip it
            	if(numberOfRows != 0) {                		
            		//serialize the line, this function also inserts it into the heap file
            		insertBusiness(line); 
                	
            	}
            	numberOfRows++;


                
            }               
            
            bufferedReader.close();
            mongo.closeConnection();
            
            
            
		}
        catch(FileNotFoundException ex) {
            System.out.println(
                "Unable to open file '" + 
                		filename + "'");                
        }
		catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getMessage());
		}
	}
	
	
	public void runQueries(String filename) {
		try {
			mongo.createConnection("DatabaseSystemsA1");
			String line = null;
			FileReader filereader = new FileReader(filename);
			
	        BufferedReader bufferedReader = new BufferedReader(filereader);                    
	        //read line by line
	    	
	        while((line = bufferedReader.readLine()) != null) {
	        	System.out.println(line);
	        	Document doc = Document.parse(line);
	            mongo.queryDatabase(doc);
	            
	        }
	        mongo.closeConnection();
	        bufferedReader.close();
	        
		}
		catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getMessage());
		}
	}	
	
}
