package Mongo;

public class MongoPopulate {
	public static void main(String[] args) {

		MongoHandler mongoHandler = new MongoHandler();
		if(args.length > 0) {
			String filename = args[0];
		    long startTime = System.nanoTime();	
		    mongoHandler.populateDatabase(filename);
		    long endTime = System.nanoTime();
		    long elapsedTime = endTime - startTime;
		    double seconds = (double) elapsedTime / 1000000000.0;
		    System.out.println("INSERT TOOK: " + seconds);				
			
		}
		else {
			System.err.println("Must enter the filename");
		}

		
	}
}
