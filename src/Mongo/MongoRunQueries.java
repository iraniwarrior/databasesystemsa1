package Mongo;

public class MongoRunQueries {
	public static void main(String[] args) {
		
		if(args.length > 0) {
			
			String queryFile = args[0];
			
			MongoHandler mongoHandler = new MongoHandler();
			queryFile = "Mongo/" +queryFile;
		    long startTime = System.nanoTime();	
		    mongoHandler.runQueries(queryFile);
		    long endTime = System.nanoTime();
		    long elapsedTime = endTime - startTime;
		    double seconds = (double) elapsedTime / 1000000000.0;
		    System.out.println("Running the Queries took: " + seconds);				
			
		}
		else {
			System.err.println("Query file argument missing");
		}		
		
	}
}
