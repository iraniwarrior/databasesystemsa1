package Mongo;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import org.bson.Document;
import org.bson.types.ObjectId;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

public class Mongo {
	
	private MongoClient mongo;
	
    // Accessing the database 
    MongoDatabase database;
    
    private MongoCollection<Document> businessCollection;
    private MongoCollection<Document> stateCollection;
    private Map <String, ObjectId> states = new HashMap <String, ObjectId>();    
	
    public MongoCollection<Document> getBusinessCollection() {
    	return businessCollection;
    }
    public MongoCollection<Document> getStateCollection() {
    	return stateCollection;
    }
    
	public Mongo() {
		java.util.logging.Logger.getLogger("org.mongodb.driver").setLevel(Level.SEVERE);
	}
	
	public void createConnection(String databaseName) {
	    // Creating a Mongo client 
	    mongo = new MongoClient( "localhost" , 27017 ); 	
	    database = mongo.getDatabase(databaseName);
	    businessCollection = database.getCollection("Business");
	    stateCollection = database.getCollection("PreviousState");
	}
	
	public Map<String, ObjectId> getStates() {
		return states;
	}
	public void closeConnection() {
	    // Close connection
		mongo.close(); 		
	}	
	
	public void populateStates() {
		//populate our state hashmap
	    Document doc = new Document ("state", "VIC");
	    stateCollection.insertOne(doc);
	    ObjectId id = (ObjectId)doc.get( "_id" );
	    states.put("VIC", id);
	    
	    doc = new Document ("state", "QLD");
	    stateCollection.insertOne(doc);
	    id = (ObjectId)doc.get( "_id" );
	    states.put("QLD", id);
	    
	    doc = new Document ("state", "NSW");
	    stateCollection.insertOne(doc);
	    id = (ObjectId)doc.get( "_id" );
	    states.put("NSW", id);
	    
	    doc = new Document ("state", "TAS");
	    stateCollection.insertOne(doc);
	    id = (ObjectId)doc.get( "_id" );
	    states.put("TAS", id);
	    
	    doc = new Document ("state", "NT");
	    stateCollection.insertOne(doc);
	    id = (ObjectId)doc.get( "_id" );
	    states.put("NT", id);
	    
	    doc = new Document ("state", "WA");
	    stateCollection.insertOne(doc);
	    id = (ObjectId)doc.get( "_id" );
	    states.put("WA", id);
	    
	    doc = new Document ("state", "ACT");
	    stateCollection.insertOne(doc);
	    id = (ObjectId)doc.get( "_id" );
	    states.put("ACT", id);
	    
	    doc = new Document ("state", "SA");
	    stateCollection.insertOne(doc);
	    id = (ObjectId)doc.get( "_id" );
	    states.put("SA", id);	    
	}
	
	public void insertObject(Document doc) {
		businessCollection.insertOne(doc);
	}   
	
	public void emptyDocument() {
		businessCollection.drop();
		stateCollection.drop();
	}
	public void queryDatabase(Document query) {
		MongoCursor<Document> cursor = businessCollection.find(query).iterator();
		
		try {
		    while (cursor.hasNext()) {
		        System.out.println(cursor.next().toJson());
		    }
		} finally {
		    cursor.close();
		}
		
	}
}
