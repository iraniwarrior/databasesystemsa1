package Heap;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class HeapQuery extends Heap {
	
	//used to keep track of when there are no more rows on the page
	private HeapFieldSizes heapFieldSizes;
	private BusinessRecord businessRecord;
	private int pageSize;
	
	public HeapQuery(int pageSize) {
		init(pageSize);
	}	
	
	public void init(int pageSize) {
		heapFieldSizes = new HeapFieldSizes();
		businessRecord = new BusinessRecord();
		this.pageSize = pageSize;
		setPageSize(pageSize);
		setHeapFile(pageSize);
	}
	
	public void runQueries(String filename) {
		try {
			String line = null;
			FileReader filereader = new FileReader(filename);
			
	        BufferedReader bufferedReader = new BufferedReader(filereader);                    
	        //read line by line
	    	String column, searchTerm, type;
	        while((line = bufferedReader.readLine()) != null) {
	            String tokenized[] = line.split("\t");
	            
	            column = tokenized[0];
	            searchTerm = tokenized[1];
	            type = "matches";
	            QueryHeap(column, searchTerm, type);
	            
	            
	        }
	        bufferedReader.close();
	        
		}
		catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getMessage());
		}
	}
	
	
	public void QueryHeap(String column, String searchTerm, String searchType) {
		byte[] page = new byte[getPageSize()];
		byte[] currentRow;
		
		try {
			
			InputStream readInputStream = new FileInputStream(getHeapFile());
			String fieldValue;
            while (readInputStream.read(page) != -1) {
            	
            	//reset the page counter (used to see how many unread bytes we have in the current page
            	int pageCounter = getPageSize();
            	
            	//get the record size
            	int recordSize = heapFieldSizes.recordSize;
            	
            	//set the page position to the beginning
            	int pagePosition = 0;
            	
            	//check if the pageCounter is greater than the row size
            	while(pageCounter > recordSize) {

            		//get the current row
            		currentRow = Arrays.copyOfRange(page, pagePosition, pagePosition+recordSize);
            		
            		//add to the page position
            		pagePosition += recordSize;
            		//deserialize the current row
            		deserialize(currentRow);
            		fieldValue = businessRecord.getRecordField(column);
            		if(fieldValue != null && !fieldValue.isEmpty()) {
            			if(searchType.equals("contains")) {
                			if(containsIgnoreCase(fieldValue, searchTerm)) {
                				businessRecord.outputObject();
                			}            				
            			}
            			else if(searchType.equals("matches")) {
            				if(fieldValue.equals(searchTerm)) {
            					businessRecord.outputObject();
            				}
            			}
            			
            		}
            		pageCounter -= recordSize;
            	}
            	pagePosition = 0;
            	pageCounter = getPageSize();                
            }
            readInputStream.close();
		}
		catch (IOException e) {
			System.err.println("Error reading file");
		}
		
	}
	
	public void deserialize(byte[] row) {
		int currentPosition = 0;
		String fieldName;
		String value = null;
		String type;
		int fieldLength;
		HeapField heapField;
		byte[] currentField;
		businessRecord.init();
		
		for(int x=1;x < heapFieldSizes.getHeapFields().length;x++) {
			heapField = heapFieldSizes.getHeapFieldByIndex(x);
			fieldName = heapField.getName();
			type = heapField.getType();
			fieldLength = heapField.getMaxLength();
			currentField = Arrays.copyOfRange(row, currentPosition, currentPosition+fieldLength);
			if(type == "String") {
				value = new String(currentField);
				value = value.trim();
				
			}

			currentPosition += fieldLength;
			
			if(type == "Long" && currentField.length > 0) {
				long number = ByteBuffer.wrap(currentField).getLong();
				if(number == 0) {
					value = "";
				}
				else {
					value = Long.toString(number);
				}
			}
			//we have our value, put it in our businessRecord Object			
			businessRecord.setRecordField(fieldName, value);
		}
		
	}
	
	public boolean containsIgnoreCase(String str, String searchStr)     {
	    if(str == null || searchStr == null) return false;

	    final int length = searchStr.length();
	    if (length == 0)
	        return true;

	    for (int i = str.length() - length; i >= 0; i--) {
	        if (str.regionMatches(true, i, searchStr, 0, length))
	            return true;
	    }
	    return false;
	}
		
}

