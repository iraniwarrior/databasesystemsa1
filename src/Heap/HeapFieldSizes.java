package Heap;

public class HeapFieldSizes {
	
	private HeapField[] heapField;
	int recordSize;
		
	public HeapField getHeapFieldByIndex(int x) {
		return heapField[x];
	}
	
	public HeapField[] getHeapFields() {
		return heapField;
	}
	public HeapFieldSizes() {
		 //- initialize the fields which will hold the type (int or string) and the max length values, the index matches to the index
		 //	  of the input file when it has been concatenated
		 heapField = new HeapField[9];
		 
		 //first one is a field we don't worry about 
		 heapField[0] = null;
		 
		 //business name
		 heapField[1] = new HeapField("businessName", "String", 200);
		 
		 //status
		 heapField[2] = new HeapField("status", "String", 12);
		 
		 //date of registration
		 heapField[3] = new HeapField("registrationDate", "String", 10);
		 
		 //date of cancellation
		 heapField[4] = new HeapField("cancellationDate", "String", 10);
		 
		 //renewal date
		 heapField[5] = new HeapField("renewalDate", "String", 10);
		 
		 //former state number
		 heapField[6] = new HeapField("formerStateNumber", "String", 10);
		 
		 //previous state
		 heapField[7] = new HeapField("previousState", "String", 3);
		 
		 //abn
		 heapField[8] = new HeapField("abn", "Long", 8);
		 
		 int totalSize = 0;
		 for(int x=0;x<heapField.length;x++) {
			 if(x != 0 ) {
	 			 totalSize += heapField[x].getMaxLength();				 
			 }

		 }
		 recordSize = totalSize;
	}
}
