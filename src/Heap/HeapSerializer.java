package Heap;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class HeapSerializer extends Heap{
	
	//- this creates objects of HeapField in an array which hold information like type (string or int) and max length
	//  The array of heapFields lines up with the exploded tab items of the data file 
	HeapFieldSizes fieldSizes;
	HeapWriter heapWriter;
	private byte[] pageByte;
	private int arrayPosition;
	
	//this is used to keep track of the amount of bytes left for paging purposes
	private int pageBytesLeft;	
	private Boolean emptyPage = true;
	
	private int numberOfPages = 0;
	private int numberOfRecords = 0;
	
	public HeapSerializer(int pageSize) {
		fieldSizes = new HeapFieldSizes(); 
		setPageSize(pageSize);
		pageBytesLeft = pageSize;
		heapWriter = new HeapWriter(pageSize);
		pageByte = new byte[pageSize];
		arrayPosition = 0;		
	}

	public void checkCurrentPage(int recordSize) throws IOException {
		
		//check if we need to write the page to the file
		if(pageBytesLeft - recordSize < 1) {
			fillRemainingPage();
			heapWriter.writePageToFile(pageByte);
			pageByte = new byte[getPageSize()];
			arrayPosition = 0;
			
			//reset the pageBytesLeft counter
			pageBytesLeft = getPageSize();
			pageBytesLeft -= recordSize;	
			emptyPage = true;
			numberOfPages++;
		}	
		else {		
			pageBytesLeft -= recordSize;
		}
	}
	
	//this is used when the page can't fit a new row
	public void fillRemainingPage() throws IOException {

		//create an empty byte array to attach to the end of this page
		byte[] emptyBytes = new byte[pageBytesLeft];

		//write to our file
		System.arraycopy(emptyBytes, 0, pageByte, arrayPosition, emptyBytes.length);
		
	}
	
	public void UpdateHeapFile(String filename) {
		
		try {
			String line = null;
			heapWriter.deleteFile();
			FileReader filereader = new FileReader(filename);
			
            BufferedReader bufferedReader = new BufferedReader(filereader);
            int numberOfRows = 0;            
            
        	//open the writer once to save time
        	heapWriter.openFileWriter();
            //read line by line
            while((line = bufferedReader.readLine()) != null) {
            	emptyPage = false;
                try {
                	//first line is just row name data so we skip it
                	if(numberOfRows != 0) {
                		
                		//this checks if we can fit the next row on the current page, if not it nulls out the rest of the bytes
                		checkCurrentPage(fieldSizes.recordSize);
                		//Tokenize the line, this function also calls HeapWriter which turns the fields into relevant byte array
                    	TokenizeLine(line);          
                    	numberOfRecords++;
                	}
                	numberOfRows++;

                }
                
                catch (IOException e) {
                	System.err.println(e.getMessage());
                }
                
            }   
            //write the remainder to the file if it is not empty
            if(emptyPage == false) {
            	heapWriter.writePageToFile(pageByte);
            	numberOfPages++;
            }
            System.out.println("Number of Records: " + numberOfRecords);
            System.out.println("Number of Pages: " + numberOfPages);
            
        	//close the writer
        	heapWriter.closeFileWriter();
        	
            bufferedReader.close();
            
		}
        catch(FileNotFoundException ex) {
            System.out.println(
                "Unable to open file '" + 
                filename + "'");                
        }		
		catch (IOException e) {
			System.out.println("Error reading file");
		}		
	}
	
	public void TokenizeLine(String line) throws IOException {
		
    	String splitLines[] =  line.split("\t", -1);
    	
    	//loop through the tokenized fields, start at 1 because the first element is not needed
    	for(int x=1;x<splitLines.length;x++) {
			//write to file passing in the current HeapField which holds the length and type, and the current column
			CreateByteArray(fieldSizes.getHeapFieldByIndex(x), splitLines[x]);
			
    	}
		
		
	}
	
	public void CreateByteArray(HeapField heapField, String column) throws IOException {

		//byte[] lineByte = line.getBytes(StandardCharsets.UTF_8);
		String type = heapField.getType();
		int maxLength = heapField.getMaxLength();
		byte[] standardBytes, sampleByte;
		if(type == "String") {
			//we create the maximum length of the column
			standardBytes =  new byte[maxLength];		
			
			//we get the byte array of the column
			sampleByte = column.getBytes(StandardCharsets.US_ASCII);
			
			//we copy the column into the column starting from the beginning
			System.arraycopy(sampleByte, 0, standardBytes, 0, sampleByte.length);
			
			//copy into our page array file
			System.arraycopy(standardBytes, 0, pageByte, arrayPosition, standardBytes.length);
			arrayPosition += maxLength;
			
			//System.out.println(value);
		}
		else if(type == "Long"){
			//column is empty so insert an empty byte array of size 4
			if(column.isEmpty() || column == null) {
				standardBytes = new byte[maxLength];
				System.arraycopy(standardBytes, 0, pageByte , arrayPosition, standardBytes.length);
				arrayPosition += maxLength;
			}
			//convert string to int then insert as int into our database
			else {
				long result = Long.parseLong(column);	
				byte[] resultBytes = ByteUtils.longToBytes(result);
				System.arraycopy(resultBytes, 0, pageByte , arrayPosition, resultBytes.length);
				arrayPosition += resultBytes.length;
				
			}
		}
		
		
	}	
	
	
}
