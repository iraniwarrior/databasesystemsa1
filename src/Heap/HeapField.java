package Heap;

public class HeapField {
	private String type;
	private String name;
	private int maxLength;
	
	public HeapField(String name, String type, int maxLength) {
		this.type = type;
		this.maxLength = maxLength;
		this.name = name;
	}
	
	public int getMaxLength() {
		return maxLength;
	}
	
	public String getType() {
		return type;
	}
	
	public String getName() {
		return name;
	}
}
