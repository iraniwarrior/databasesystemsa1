package Heap;

public class HeapRunQueries {
	public static void main(String[] args) {
		
		if(args.length == 2) {
			
			String pagesizeString = args[0];			
			int pageSize = Integer.parseInt(pagesizeString);		
			HeapQuery heapQuery = new HeapQuery(pageSize);			
			String queryFile = args[1];
			long startTime = System.nanoTime();
			queryFile = "Heap/" +queryFile;
		    heapQuery.runQueries(queryFile);
		    long endTime = System.nanoTime();
		    long elapsedTime = endTime - startTime;
		    double seconds = (double) elapsedTime / 1000000000.0;
		    System.out.println("Running the Queries took: " + seconds);	
		}
		else {
			
			System.out.println("Error need to input filename");
				
		}
	}
}
