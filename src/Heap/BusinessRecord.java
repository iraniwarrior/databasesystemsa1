package Heap;

public class BusinessRecord {
	private String businessName;
	private String status;
	private String registrationDate;
	private String cancellationDate;
	private String renewalDate;
	private String formerStateNumber;
	private String previousState;
	private String abn;
	
	public void outputObject() {
		if(!businessName.isEmpty()) {
			System.out.print(businessName + " ");
		}
		if(!status.isEmpty()) {
			System.out.print(status + " ");
		}
		if(!registrationDate.isEmpty()) {
			System.out.print(registrationDate + " ");
		}
		if(!cancellationDate.isEmpty()) {
			System.out.print(cancellationDate + " ");
		}
		if(!renewalDate.isEmpty()) {
			System.out.print(renewalDate + " ");
		}
		if(!formerStateNumber.isEmpty()) {
			System.out.print(formerStateNumber + " ");
		}
		if(!previousState.isEmpty()) {
			System.out.print(previousState + " ");
		}
		if(!abn.isEmpty()) {
			System.out.print(abn + " ");
		}		
		System.out.println();
	}
	
	public BusinessRecord() {
		init();
	}
	
	public void init() {
		this.businessName = null;
		this.status = null;
		this.registrationDate = null;
		this.cancellationDate = null;
		this.renewalDate = null;
		this.formerStateNumber = null;
		this.previousState = null;
		this.abn = null;
	}
	
	public void setRecordField(String fieldName, String value) {
		switch (fieldName) {
			case "businessName" : 
				this.businessName = value;				
				break;
			case "status" : 
				this.status = value;				
				break;
			case "registrationDate" : 
				this.registrationDate = value;				
				break;
			case "cancellationDate" : 
				this.cancellationDate = value;				
				break;
			case "renewalDate" : 
				this.renewalDate = value;				
				break;
			case "formerStateNumber" : 
				this.formerStateNumber = value;				
				break;
			case "previousState" :
				this.previousState = value;
				break;
			case "abn" :
				this.abn = value;
				break;
		}
	}
	
	public String getRecordField(String fieldName) {
		switch (fieldName) {
			case "businessName" : 
				return this.businessName;				
			case "status" : 
				return this.status;				
			case "registrationDate" : 
				return this.registrationDate;				
			case "cancellationDate" : 
				return this.cancellationDate;				
			case "renewalDate" : 
				return this.renewalDate;				
			case "formerStateNumber" : 
				return this.formerStateNumber;				
			case "previousState" :
				return this.previousState;
			case "abn" :
				return this.abn;
			default :
				return null;
		}		
	}

	public String getBusinessName() {
		return businessName;
	}

	public String getStatus() {
		return status;
	}

	public String getRegistrationDate() {
		return registrationDate;
	}

	public String getCancellationDate() {
		return cancellationDate;
	}

	public String getRenewalDate() {
		return renewalDate;
	}

	public String getFormerStateNumber() {
		return formerStateNumber;
	}

	public String getPreviousState() {
		return previousState;
	}

	public String getAbn() {
		return abn;
	}
	
}
