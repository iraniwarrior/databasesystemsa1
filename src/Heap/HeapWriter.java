package Heap;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class HeapWriter extends Heap {
	
	private FileOutputStream fos = null;
	private DataOutputStream dos = null;
	
	
	public HeapWriter(int pageSize) {
		setPageSize(pageSize);
		setHeapFile(pageSize);
	}
	
	public void deleteFile() {
		File file = new File(getHeapFile());
		file.delete();		
	}
	
	
	
	public void openFileWriter() throws FileNotFoundException {
		fos = new FileOutputStream(getHeapFile(), true);
		dos = new DataOutputStream(fos);
	}
	
	public void closeFileWriter() throws IOException {
		dos.close();
		fos.close();	
	}
	

	
//	public void WriteColumnToFile(HeapField heapField, String column) throws IOException {
//
//		//byte[] lineByte = line.getBytes(StandardCharsets.UTF_8);
//		String type = heapField.getType();
//		int maxLength = heapField.getMaxLength();
//		if(type == "String") {
//			//we create the maximum length of the column
//			byte[] standardBytes =  new byte[maxLength];		
//			
//			//we get the byte array of the column
//			byte[] sampleByte = column.getBytes(StandardCharsets.US_ASCII);
//			
//			//we copy the column into the column starting from the beginning
//			System.arraycopy(sampleByte, 0, standardBytes, 0, sampleByte.length);
//			
//			//write to our file
//			fos.write(standardBytes);			
//		}
//		else if(type == "Long"){
//			//column is empty so insert an empty byte array of size 4
//			if(column.isEmpty() || column == null) {
//				byte[] standardBytes = new byte[maxLength];
//				dos.write(standardBytes);
//			}
//			//convert string to int then insert as int into our database
//			else {
//				long result = Long.parseLong(column);		
//				dos.writeLong(result);	
//			}
//		}
//		
//	}
	
	public void writePageToFile(byte[] pageBytes) throws IOException {
		fos.write(pageBytes);
	}
	
	

}


