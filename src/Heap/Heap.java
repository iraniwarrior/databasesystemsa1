package Heap;

public class Heap {
	private int pageSize;
	private String heapFile = "heap";
	
	public int getPageSize() {
		return pageSize;
	}
	
	public String getHeapFile() {
		return heapFile;
	}
	
	public void setHeapFile(int pagesize) {
		this.heapFile += "."+pagesize;
	}
	
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
}
