import Heap.HeapQuery;


public class dbquery {
	public static void main(String[] args) {
		
		if(args.length == 2) {
			try {
				String queryText = args[0];
				String queryField = "businessName";
				String pagesizeString = args[1];
				int pageSize = Integer.parseInt(pagesizeString);
				
				HeapQuery heapQuery = new HeapQuery(pageSize);
				long startTime = System.nanoTime();
				heapQuery.QueryHeap(queryField, queryText, "contains");
			    long endTime = System.nanoTime();
			    long elapsedTime = endTime - startTime; 
//			    double milliseconds = (double) elapsedTime / 1000.0;
//			    double seconds = milliseconds / 1000;
			    double microSeconds = elapsedTime / 1e3;
			    double milliSeconds = microSeconds / 1e3;
			    System.out.println("Time Taken: " + milliSeconds + "ms");				
				
			}
			catch (Exception e) {
				e.printStackTrace();
				System.err.println(e.getMessage());
			}

		}
		else {
			System.err.println("Missing arguments: required format -p 4096 datafile");
		}

	}
}
