package Derby;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;

public class Derby {
	
    private static String dbURL = "jdbc:derby://localhost:1527/Assignment1;create=true";
    private static String businessTable = "Business";
    private String stateTable = "PreviousState";
    private String registrationTable = "RegistrationDetails";
    // jdbc Connection
    private static Connection conn = null;
    private static Statement stmt = null;
    private static PreparedStatement preparedStmt = null;
    private static ResultSet resultSet = null;
    
    public void dropTable(String table) throws SQLException {
    	try {
    		stmt.execute("DROP TABLE " + table);	
    	}
    	catch (Exception e){
    		System.err.println("Table " +table+ " doesn't exist");
    	}
		
    }
    
    public void createBusinessTable() throws SQLException {
        stmt.execute("CREATE TABLE "+businessTable+
        		"    (id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),\r\n " + 
        		"    businessName VARCHAR(200),"
        		+ "  formerStateNumber VARCHAR(10),"
        		+ "  previousStateId INT,"
        		+ "  abn BIGINT)");  	
    }
    
    public void createStateTable() throws SQLException {
        stmt.execute("CREATE TABLE "+stateTable+
        		"    (id INT PRIMARY KEY, " + 
        		"    State VARCHAR(3))");  	
    } 
    
    public void createRegistrationTable() throws SQLException {
        stmt.execute("CREATE TABLE "+registrationTable+
        		"    (id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),\r\n " +         		
        		"    businessId INT,"
        		+ "  status VARCHAR(12),"
        		+ "  registrationDate DATE,"
        		+ "  cancellationDate DATE,"
        		+ "  renewalDate DATE)");
    }     
    
    public void insertRecord(String query) throws SQLException {
        stmt.execute(query);    	
    }
    
    public int insertNewRecord(String query) throws SQLException {
		//insertRecord);
        preparedStmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        preparedStmt.executeUpdate();
        resultSet = preparedStmt.getGeneratedKeys();
        if(resultSet.next()) {
	        int insertId = resultSet.getInt(1);
	        return insertId;	              	
        }
        else {
        	return -1;
        }
        
    }    
    
	public void insertRegistrationDetails(int insertId, String status, Date registrationDate,
		Date cancellationDate, Date renewalDate) throws SQLException {
		
		Timestamp regStamp = null;
		Timestamp cancStamp = null;
		Timestamp renStamp = null;
	    
		if(registrationDate != null) {
			regStamp = new java.sql.Timestamp(registrationDate.getTime());		
		}
		if(cancellationDate != null) {
			cancStamp = new java.sql.Timestamp(cancellationDate.getTime());			
		}
		if(renewalDate != null) {
			renStamp = new java.sql.Timestamp(renewalDate.getTime());			
		}		

    	String query = "INSERT INTO RegistrationDetails "
    			+ "(businessId, status, registrationDate, cancellationDate, renewalDate) "
    			+ "VALUES (?, ?, ?, ?, ?)";
		preparedStmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		preparedStmt.setInt(1,  insertId);
		preparedStmt.setString(2, status);
		preparedStmt.setTimestamp(3, regStamp);
		preparedStmt.setTimestamp(4, cancStamp);
		preparedStmt.setTimestamp(5, renStamp);		

		preparedStmt.executeUpdate();
	}    
    
    public void queryDatabase(String query) throws SQLException {
        ResultSet results = stmt.executeQuery(query);
        ResultSetMetaData rsmd = results.getMetaData();
        int columnsNumber = rsmd.getColumnCount();
        
        while(results.next())
        {
        	System.out.println();
            for (int i = 1; i <= columnsNumber; i++) {
                if (i > 1) System.out.print(",  ");
                String columnValue = results.getString(i);
                System.out.print(rsmd.getColumnName(i) + ":" +columnValue);
            }
        }    	
    }
    
    public void createConnection() throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException
    {
        Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
        //Get a connection
        conn = DriverManager.getConnection(dbURL); 
        stmt = conn.createStatement();
                
    }
    
    public void closeConnection() throws SQLException {
    	
		stmt.close();
    }

}
