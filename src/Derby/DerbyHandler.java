package Derby;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import Heap.BusinessRecord;
import Heap.HeapField;
import Heap.HeapFieldSizes;

public class DerbyHandler {
	
	private FileOutputStream fos = null;
	private DataOutputStream dos = null;
	private Derby derby = new Derby();
	
	private Map <String, Integer> states = new HashMap <String, Integer>();
	
	public DerbyHandler() {
		states.put("VIC", 1);
		states.put("ACT", 2);
		states.put("NSW", 3);
		states.put("NT", 4);
		states.put("QLD", 5);
		states.put("WA", 6);
		states.put("TAS", 7);
		states.put("SA", 8);
	}
	
	public void populateStates() throws SQLException {
		String query;
		for(String key : states.keySet()) {
			int id = states.get(key);
			query = "INSERT INTO PreviousState (id, State)"
					+ "VALUES ("+id+", '"+key+"')";
			derby.insertRecord(query);
			
		}
	}
	
	public void writeToFile(String line) throws IOException {
		//write to our file
		dos.writeChars(line);
	}
	
	public void createTables() throws SQLException {
        
		derby.dropTable("Business");
		derby.dropTable("PreviousState");
		derby.dropTable("RegistrationDetails");
		derby.createBusinessTable();
		derby.createStateTable();
		derby.createRegistrationTable();
		populateStates();
	}
	
	public void populateDatabase(String filename) {
		
		try {
			String line = null;
			FileReader filereader = new FileReader(filename);
			
			derby.createConnection();
			createTables();
			
            BufferedReader bufferedReader = new BufferedReader(filereader);
            int numberOfRows = 0;            
            

        	
            //read line by line
            while((line = bufferedReader.readLine()) != null) {
                
                try {
                	//first line is just row name data so we skip it
                	if(numberOfRows != 0) {                		
                		//serialize the line, this function also inserts it into the heap file
                		createDerbyQuery(line); 
                    	
                	}
                	numberOfRows++;

                }
                catch (IOException e) {
                	System.err.println(e.getMessage());
                }
                
            }   
            
            //derby.queryDatabase("select * from RegistrationDetails");
        	
            bufferedReader.close();
            derby.closeConnection();
            
		}
        catch(FileNotFoundException ex) {
            System.out.println(
                "Unable to open file '" + 
                		filename + "'");                
        }		
		catch (IOException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace();			
		}
	}
	
	
	public void runQueries(String filename) {
		try {
			String line = null;
			FileReader filereader = new FileReader(filename);
			
	        BufferedReader bufferedReader = new BufferedReader(filereader);                    
	        //read line by line
	    	
	        derby.createConnection();
	        while((line = bufferedReader.readLine()) != null) {
	            derby.queryDatabase(line);
	            
	        }
	        derby.closeConnection();
	        bufferedReader.close();
	        
		}
		catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getMessage());
		}
	}
	
	public Date convertAusToSQLDate(String incomingDate) throws ParseException {		
        DateFormat ausDate = new SimpleDateFormat("dd/MM/YYYY");
        ausDate.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = ausDate.parse(incomingDate);
	    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
	    dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        incomingDate = dateFormat.format(date);
	    Date parsedDate = dateFormat.parse(incomingDate);
	    return parsedDate;
	}
	
	public void createDerbyQuery(String line) throws Exception {
		
    	String splitLines[] =  line.split("\t", -1);
    	HeapFieldSizes heapFieldSizes = new HeapFieldSizes();
    	HeapField heapField = null;
    	String fieldName = "";
    	BusinessRecord businessRecord = new BusinessRecord();
    	//loop through the tokenized fields, start at 1 because the first element is not needed    	
    	for(int x=0;x<splitLines.length;x++) {
    		//skip column 0 as that is irrelevant information
    		if(x != 0 ) {    		
    			//Get the heapField which stores information about the current tokenized index)
    			heapField = heapFieldSizes.getHeapFieldByIndex(x);
    			//get the fieldName
    			fieldName = heapField.getName();
    			//we have our value, put it in our businessRecord Object			
    			businessRecord.setRecordField(fieldName, splitLines[x]);
    			
    		}
    	}
    	//create the insert string
    	String abn = businessRecord.getAbn();
    	String formerStateNumber = businessRecord.getFormerStateNumber();
    	String previousState = businessRecord.getPreviousState();
    	String status = businessRecord.getStatus();
    	String registrationDate = businessRecord.getRegistrationDate();
    	String cancellationDate = businessRecord.getCancellationDate();
    	String renewalDate = businessRecord.getRenewalDate();    	
    	
    	Date regDate = null;
    	Date cancDate = null;
    	Date renDate = null;    	
    	
    	Long longABN = null;
    	
    	Integer intPrevState = null;
    	
    	if(abn != null && !abn.isEmpty()) {
    		longABN = Long.parseLong(abn);
    	}
    	
    	//this replaces the prev state string to the state Id from our database
    	if(previousState != null && !previousState.isEmpty()) {
    		previousState = previousState.toUpperCase();
    		intPrevState = states.get(previousState);
    	}
    	
    	if(registrationDate != null && !registrationDate.isEmpty()) {
//    		System.out.println("OLD " + registrationDate );
    		regDate = convertAusToSQLDate(registrationDate);
    //		System.out.println("NEW " + registrationDate);
    	}
    	
    	if(cancellationDate != null && !cancellationDate.isEmpty()) {
    		cancDate = convertAusToSQLDate(cancellationDate);
    	}
    	
    	if(renewalDate != null && !renewalDate.isEmpty()) {
    		renDate = convertAusToSQLDate(renewalDate);
    	}    	
    	
    	

    	String query = "INSERT INTO Business "
    			+ "(businessName, formerStateNumber, previousStateId, abn) "
    			+ "VALUES ('"+escapeSQL(businessRecord.getBusinessName())+"', "
    			+ "'"+formerStateNumber+"', "
    			+ intPrevState+", "
    			+ longABN+")";
    	int insertId = derby.insertNewRecord(query);
    	
    	if(insertId >= 0) {
    		//insert the registration details here
        	derby.insertRegistrationDetails(insertId, status, regDate, cancDate, renDate);
    	}
    	else {
    		throw new Exception("Could not insert query: " + query);
    	}
		
	}
	
	public static String escapeSQL(String text) {
        return text
                .replace("\'", "\''");
                //And the rest of MUST escape characters
    }	
}
