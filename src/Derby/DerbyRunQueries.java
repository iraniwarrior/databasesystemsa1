package Derby;

public class DerbyRunQueries {
	public static void main(String[] args) {
		DerbyHandler derbyHandler = new DerbyHandler();
		
	  
	    	
		
		if(args.length > 0) {
			String queryFile = args[0];
			long startTime = System.nanoTime();
			queryFile = "Derby/" +queryFile;
		    derbyHandler.runQueries(queryFile);
		    long endTime = System.nanoTime();
		    long elapsedTime = endTime - startTime;
		    double seconds = (double) elapsedTime / 1000000000.0;
		    System.out.println("Running the Queries took: " + seconds);	
		}
		else {
			
			System.out.println("Error need to input filename");
				
		}
	}
}
