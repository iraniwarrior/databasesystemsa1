package Derby;

public class DerbyPopulate {
	public static void main(String[] args) {
		
		if(args.length > 0) {
			String filename = args[0];
			
			DerbyHandler derbyHandler = new DerbyHandler();
			
		    long startTime = System.nanoTime();	
		    derbyHandler.populateDatabase(filename);
		    long endTime = System.nanoTime();
		    long elapsedTime = endTime - startTime; 
		    double seconds = (double) elapsedTime / 1000000000.0;
		    System.out.println("INSERT TOOK: " + seconds);		    
		
		}			
		
	}
}
