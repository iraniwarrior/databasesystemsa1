import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import org.bson.Document;

import com.mongodb.util.JSON;

public class GenerateQueries {
	private String filename = "BUSINESS_NAMES_201803.csv";
	private PrintWriter pwDerbyA = null;
	private PrintWriter pwDerbyB = null;
	private PrintWriter pwMongoA = null;
	private PrintWriter pwMongoB = null;
	private PrintWriter pwHeapA = null;
	private PrintWriter pwHeapB = null;	
	
	private BufferedWriter bwDerbyA = null;
	private BufferedWriter bwDerbyB = null;
	private BufferedWriter bwMongoA = null;
	private BufferedWriter bwMongoB = null;
	private BufferedWriter bwHeapA = null;
	private BufferedWriter bwHeapB = null;
	
	private String derbyQueryA = "Derby/derbyQueriesA";
	private String derbyQueryB = "Derby/derbyQueriesB";
	private String mongoQueryA = "Mongo/mongoQueriesA";
	private String mongoQueryB = "Mongo/mongoQueriesB";
	private String heapQueryA = "Heap/heapQueriesA";
	private String heapQueryB = "Heap/heapQueriesB";	

	public void openFileWriter() throws IOException {

		pwDerbyA = new PrintWriter(derbyQueryA);
		pwDerbyB = new PrintWriter(derbyQueryB);
		pwMongoA = new PrintWriter(mongoQueryA);
		pwMongoB = new PrintWriter(mongoQueryB);
		pwHeapA = new PrintWriter(heapQueryA);
		pwHeapB = new PrintWriter(heapQueryB);
		
		bwDerbyA = new BufferedWriter(pwDerbyA);
		bwDerbyB = new BufferedWriter(pwDerbyB);
		bwMongoA = new BufferedWriter(pwMongoA);
		bwMongoB = new BufferedWriter(pwMongoB);
		bwHeapA = new BufferedWriter(pwHeapA);
		bwHeapB = new BufferedWriter(pwHeapB);		
	}
	
	public void closeFileWriter() throws IOException {
		bwDerbyA.close();
		bwDerbyB.close();
		bwMongoA.close();
		bwMongoB.close();
		bwHeapA.close();
		bwHeapB.close();		
	}
	
	public void createQueries() {		
        
		try {
			String line = null;
			FileReader filereader = new FileReader(filename);
			openFileWriter();
			
            BufferedReader bufferedReader = new BufferedReader(filereader);
            int numberOfRows = 0;                        
        	
            //read line by line
            int numberOfQueries = 0;
            while((line = bufferedReader.readLine()) != null) {
                
                try {
                	//first line is just row name data so we skip it
                	if(numberOfRows != 0) {                		
                		//serialize the line, this function also inserts it into the heap file
                		//create query for every 100th line
                		if(numberOfRows % 100000 == 0) {
                			System.out.println(line);
                			createQueryLine(line);
                			numberOfQueries++;
                		}
                    	
                	}
                	numberOfRows++;

                }
                catch (IOException e) {
                	System.err.println(e.getMessage());
                	e.printStackTrace();
                }
                catch (Exception e) {
                	System.err.println(e.getMessage());
                	e.printStackTrace();
                }
                
            }
            System.out.println("Number of queries: " + numberOfQueries);
            bufferedReader.close();
            closeFileWriter();
		}
        catch (Exception e ){
        	System.err.println(e.getMessage());
        	e.printStackTrace();
        }
            
	}
	
	public void createQueryLine(String line) throws IOException {
		String splitLines[] =  line.split("\t", -1);
		String businessName = splitLines[1];
		
		String derbyQueryA = "SELECT * from Business WHERE businessName = '"+escapeSQL(businessName)+"'\n";
//		String derbyQueryB = "SELECT * from Business "
//				+ "LEFT OUTER JOIN RegistrationDetails "
//				+ "ON Business.id = RegistrationDetails.businessId "
//				+ "WHERE RegistrationDetails.status = 'Deregistered' \n";		
		
	    Document mongoDocA = new Document ("businessName", businessName);
	    String mongoQueryA = JSON.serialize(mongoDocA);
	    mongoQueryA += "\n";
		
	    Document mongoDocB = new Document("RegistrationDetails", new Document("status", "Deregistered"));
//	    String mongoQueryB = JSON.serialize(mongoDocB);
//	    mongoQueryB += "\n";
	    
	    String heapQueryA = "businessName\t"+businessName+"\n";
	    //String heapQueryB = "status\tDeregistered\n";
	    
	    
	    bwDerbyA.write(derbyQueryA);
	    //bwDerbyB.write(derbyQueryB);
	    bwMongoA.write(mongoQueryA);
	    //bwMongoB.write(mongoQueryB);
	    bwHeapA.write(heapQueryA);
	    //bwHeapB.write(heapQueryB);
	    
	}	
	
	public static void main(String[] args) {
		GenerateQueries generateQueries = new GenerateQueries();
		generateQueries.createQueries();
	}
	
	public static String escapeSQL(String text) {
        return text
                .replace("\'", "\''");
                //And the rest of MUST escape characters
    }		
	
	
}
