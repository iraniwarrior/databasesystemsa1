package csvParser;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;



public class InputReader {
	
	
	
	public static String mongoFile = "MongoQueries";
	public static String derbyFile = "DerbyQueries";
	public static String inputfile = "BUSINESS_NAMES_201803_SUBSET.csv";
	
	
	
	public static void main(String[] args) {

		
		String line = null;
		String splitLines[];
		
		
		try {
			FileReader filereader = new FileReader(InputReader.inputfile);
			
            BufferedReader bufferedReader = 
                new BufferedReader(filereader);
            String derbyInsertOutput = "TEST";
            FileWriter fileWriter = new FileWriter(derbyInsertOutput);
            PrintWriter printWriter = new PrintWriter(fileWriter);
           
            while((line = bufferedReader.readLine()) != null) {
            	printWriter.write(line);
            	splitLines =  line.split("\\t");
            	
            	for(int x=0;x<splitLines.length;x++) {
            		//System.out.print(splitLines[x]);
            	}
               
                
            }   

            bufferedReader.close();
            printWriter.close();
            
            
		}
        catch(FileNotFoundException ex) {
            System.out.println(
                "Unable to open file '" + 
                InputReader.inputfile + "'");                
        }		
		catch (IOException e) {
			System.out.println("Error reading file");
		}
	}
}
