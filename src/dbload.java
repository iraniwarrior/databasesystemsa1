import Heap.HeapSerializer;


public class dbload {
	public static void main(String[] args) {
		
		if(args.length == 3) {
			try {
				int pagesize = Integer.parseInt(args[1]);
				String filename = args[2];
				HeapSerializer heapSerializer = new HeapSerializer(pagesize);
				
			    long startTime = System.nanoTime();				
				heapSerializer.UpdateHeapFile(filename);
			    long endTime = System.nanoTime();
			    long elapsedTime = endTime - startTime; 
//			    double milliseconds = (double) elapsedTime / 1000.0;
//			    double seconds = milliseconds / 1000;
			    double microSeconds = elapsedTime / 1e3;
			    double milliSeconds = microSeconds / 1e3;
			    System.out.println("Time Taken: " + milliSeconds + "ms");
			}
			catch (Exception e) {
				e.printStackTrace();
				System.err.println(e.getMessage());
			}

		}
		else {
			System.err.println("Missing arguments: required format -p 4096 datafile");
		}

	}
}
